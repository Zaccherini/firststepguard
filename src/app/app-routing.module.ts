import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { RoleGuard } from './features/guards/role.guard';

const routes: Routes = [
  { path: '', redirectTo: 'login',  pathMatch: 'full' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginModule' },
  { path: 'registration', loadChildren: './pages/registration/registration.module#RegistrationModule' },
  {
    path: 'mainPage',
    loadChildren: './pages/main-page/main-page.module#MainPageModule',
    canActivate: [RoleGuard],
  },
  { path: '**', redirectTo: 'login' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
