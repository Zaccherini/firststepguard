import { Injectable } from '@angular/core';
import {
  HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse
} from '@angular/common/http';

import { Observable } from 'rxjs';
import { User } from '../../model/user';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class HttpRequestInterceptor implements HttpInterceptor {

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    if(req.method.toLowerCase() == 'post' && req.body instanceof User) {
      console.log('The request has a user as body');
      let body = req.body;
      body.setToken(`${req.body.username}:${req.body.password}`);
      body.setRoles(['doctor']);
      req = req.clone( {
        body: body,
      });
    }

    return next.handle(req);
  }
}
