export class User {

  constructor(
    public username: string,
    public mail: string,
    public password: string,
    public profileImageName: string,
    public roles?: string[],
    public token?: string,
    public id?: number,
  ) { }

  public setToken(token: string) {
    this.token = token;
  }

  public setRoles(roles: string[]) {
    this.roles = roles;
  }

}

