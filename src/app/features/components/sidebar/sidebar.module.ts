import { NgModule } from '@angular/core';
import { SidebarComponent } from './sidebar.component';
import { CommonModule } from '@angular/common';
import { SidebarContentPatientComponent } from './sidebar-content-patient/sidebar-content-patient.component';
import { SidebarContentDoctorComponent } from './sidebar-content-doctor/sidebar-content-doctor.component';
import { SidebarContentAdminComponent } from './sidebar-content-admin/sidebar-content-admin.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [
    SidebarComponent,
    SidebarContentPatientComponent,
    SidebarContentDoctorComponent,
    SidebarContentAdminComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports: [
    SidebarComponent,
  ]
})
export class SidebarModule { }
