import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarContentDoctorComponent } from './sidebar-content-doctor.component';

describe('SidebarContentDoctorComponent', () => {
  let component: SidebarContentDoctorComponent;
  let fixture: ComponentFixture<SidebarContentDoctorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarContentDoctorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarContentDoctorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
