import { Component, OnInit, Output, EventEmitter, Input, OnChanges } from '@angular/core';
import { UserClientService } from 'src/app/services/user-client/user-client.service';
import { User } from '../../model/user';
import { ImageManagerService } from 'src/app/services/image-manager/image-manager.service';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit, OnChanges {

  @Input() inputOptions : { showNavbar: boolean, hideSidebar: boolean };
  @Output() hideNavbar = new EventEmitter<any>();

  showSidebar: boolean = false;
  profileImage = null;
  currentUser: User;

  constructor(
      private imageManager: ImageManagerService,
    ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(sessionStorage.getItem('user'));
    this.imageManager.loadImageFromServer().pipe(
      tap( image => {
        this.profileImage = image;
      })).subscribe();
  }

  ngOnChanges() {
    if (this.inputOptions && this.inputOptions.hideSidebar && this.inputOptions.showNavbar) {
      this.showSidebar = false;
    }
  }

  show() {
    this.showSidebar = !this.showSidebar;
    if (this.showSidebar) {
      let hidingOptions = { showSidebar: this.showSidebar, hideNavbar: true }
      this.hideNavbar.emit(hidingOptions);
      return;
    }
    let hidingOptions = { showSidebar: this.showSidebar, hideNavbar: false }
    this.hideNavbar.emit(hidingOptions);
  }

  getCurrentRole(): string {
    return sessionStorage.getItem('currentRole');
  }

  hideSidebar() {
    this.showSidebar = false;
  }

}
