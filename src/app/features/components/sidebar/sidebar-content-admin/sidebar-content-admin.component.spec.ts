import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarContentAdminComponent } from './sidebar-content-admin.component';

describe('SidebarContentAdminComponent', () => {
  let component: SidebarContentAdminComponent;
  let fixture: ComponentFixture<SidebarContentAdminComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarContentAdminComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarContentAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
