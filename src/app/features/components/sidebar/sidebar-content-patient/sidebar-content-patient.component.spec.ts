import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SidebarContentPatientComponent } from './sidebar-content-patient.component';

describe('SidebarContentPatientComponent', () => {
  let component: SidebarContentPatientComponent;
  let fixture: ComponentFixture<SidebarContentPatientComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SidebarContentPatientComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SidebarContentPatientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
