import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-top-right-buttons',
  templateUrl: './top-right-buttons.component.html',
  styleUrls: ['./top-right-buttons.component.scss']
})
export class TopRightButtonsComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  goToLogin() {
    this.router.navigate(['login']);
  }

  goToRegistration() {
    this.router.navigate(['registration']);
  }
}
