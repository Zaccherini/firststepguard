import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TopRightButtonsComponent } from './top-right-buttons.component';

describe('TopRightButtonsComponent', () => {
  let component: TopRightButtonsComponent;
  let fixture: ComponentFixture<TopRightButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TopRightButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TopRightButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
