import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopRightButtonsComponent } from './top-right-buttons.component';



@NgModule({
  declarations: [TopRightButtonsComponent],
  imports: [
    CommonModule
  ],
  exports: [
    TopRightButtonsComponent
  ]
})
export class TopRightButtonsModule { }
