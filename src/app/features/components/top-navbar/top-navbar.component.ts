import { Component, OnInit, Input, OnChanges, Output, EventEmitter } from '@angular/core';
import { LoggerService } from 'src/app/services/logger/logger.service';
import { User } from '../../model/user';

@Component({
  selector: 'app-top-navbar',
  templateUrl: './top-navbar.component.html',
  styleUrls: ['./top-navbar.component.scss']
})
export class TopNavbarComponent implements OnInit, OnChanges {

  @Input() inputOptions : { showSidebar: boolean, hideNavbar: boolean };
  @Output() hideSidebar = new EventEmitter<any>();

  navbarOpen: boolean = false;
  currentUser: User;
  currentRole: string;

  constructor(
    private logger: LoggerService,
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(sessionStorage.getItem('user'));
    this.currentRole = sessionStorage.getItem('currentRole');
  }

  toggleNavbar() {
    this.navbarOpen = !this.navbarOpen;
    if (this.navbarOpen) {
      let hidingOptions = { showNavbar: this.navbarOpen, hideSidebar: true }
      this.hideSidebar.emit(hidingOptions);
      return;
    }
    let hidingOptions = { showNavbar: this.navbarOpen, hideSidebar: false }
    this.hideSidebar.emit(hidingOptions);
  }

  logout() {
    this.logger.logout();
  }

  rolesContain(role: string): boolean {
    let goOn = true;
    this.currentUser.roles.forEach(userRole => {
      if (goOn) {
        if (this._userIsAlso(role, userRole)) {
          goOn = false;
        }
      }
    });
    return !goOn;
  }

  private _userIsAlso(role: string, userRole: string): boolean {
    return (userRole == role) && userRole != this.currentRole;
  }

  changeRole(newRole: string) {
    this.currentRole = newRole;
    sessionStorage.setItem('currentRole', newRole);
  }

  ngOnChanges() {
    if (this.inputOptions && this.inputOptions.hideNavbar && this.inputOptions.showSidebar) {
      this.navbarOpen = false;
    }
  }
}
