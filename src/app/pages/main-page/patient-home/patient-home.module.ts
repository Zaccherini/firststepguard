import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { PatientHomeComponent } from './patient-home.component';

const routes: Routes = [
  { path: '', component: PatientHomeComponent}
]

@NgModule({
  declarations: [
    PatientHomeComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
  ]
})
export class PatientHomeModule { }
