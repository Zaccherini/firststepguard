import { Component, OnInit, OnChanges } from '@angular/core';
import { LoggerService } from 'src/app/services/logger/logger.service';
import { Router, Event, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-home',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})
export class MainPageComponent implements OnInit {

  roles: string[];
  sidebarOptions : { showSidebar: boolean, hideNavbar: boolean };
  navbarOptions : { showNavbar: boolean, hideSidebar: boolean };

  constructor(
    private logger: LoggerService,
    private router: Router,
  ) { }

  ngOnInit() {
    this.router.events.subscribe(
    (event: Event) => {
            if (event instanceof NavigationEnd) {
                this._managmentChildNavigation()
            }
    });
  }

  _managmentChildNavigation() {
    if (!this._isInitialNavigation()) {
      return;
    }
    try {
        switch (sessionStorage.getItem('currentRole')) {
        case 'admin':  {
          this.router.navigate(['mainPage/admin-home']);
          break;
        }
        case 'doctor':  {
          this.router.navigate(['mainPage/doctor-home']);
          break;
        }
        case 'patient':  {
          this.router.navigate(['mainPage/patient-home']);
          break;
        }
        default: {
          this.router.navigate(['login']);
        }
      }
    } catch (error) {
      this.logger.logout();
    }
  }

  _isInitialNavigation(): boolean {
    return this.router.url == '/mainPage';
  }

  sendSidebarOptions(options : { showSidebar: boolean, hideNavbar:boolean }) {
    this.sidebarOptions = options;
  }

  sendNavbarOptions(options : { showNavbar: boolean, hideSidebar:boolean }) {
    this.navbarOptions = options;
  }
}
