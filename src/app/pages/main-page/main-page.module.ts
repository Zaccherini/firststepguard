import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { MainPageComponent } from './main-page.component'
import { TopNavbarComponent } from 'src/app/features/components/top-navbar/top-navbar.component'
import { CommonAcrossHomesModule } from 'src/app/shared/common-across-homes/common-across-homes.module';


const route: Routes = [
  { path: '',
    component: MainPageComponent,
    children: [
      { path: 'admin-home', loadChildren: './admin-home/admin-home.module#AdminHomeModule' },
      { path: 'doctor-home', loadChildren: './doctor-home/doctor-home.module#DoctorHomeModule' },
      { path: 'patient-home', loadChildren: './patient-home/patient-home.module#PatientHomeModule' },
      { path: 'profile', loadChildren: '../profile/profile.module#ProfileModule' },
    ]
  },
]

@NgModule({
  declarations: [
    MainPageComponent,
    TopNavbarComponent,
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(route),
    CommonAcrossHomesModule,
  ]
})
export class MainPageModule { }
