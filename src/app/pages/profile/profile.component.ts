import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { User } from 'src/app/features/model/user';
import { UserClientService } from 'src/app/services/user-client/user-client.service';
import { confirmPasswordValidator } from 'src/app/features/validators/confirm-password-validator';
import { ImageManagerService } from 'src/app/services/image-manager/image-manager.service';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  currentUser: User;
  updateDataForm: FormGroup;
  profileImage;
  attemptToSubmit: boolean;
  modifing: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserClientService,
    private imageManager: ImageManagerService,
  ) { }

  ngOnInit() {
    this.currentUser = JSON.parse(sessionStorage.getItem('user'));
    this.updateDataForm = this.formBuilder.group( {
      username: [{ value: this.currentUser.username , disabled: true }],
      mail: [{ value: this.currentUser.mail, disabled: true }],
      password: [{ value: this.currentUser.password, disabled: true }],
      confirm: [{ value: this.currentUser.password, disabled: true }],
      profileImg: [],
    },
    {
      validators: [ confirmPasswordValidator() ]
    })
    this._setImage();
  }

  get username(){ return this.updateDataForm.get('username') };
  get mail() { return this.updateDataForm.get('mail') };
  get password() { return this.updateDataForm.get('password') };
  get confirm() { return this.updateDataForm.get('confirm') };
  get profileImg() { return this.updateDataForm.get('profileImg') };

  set usernameValue (username: string) { this.updateDataForm.get('username').setValue(username) };
  set mailValue (username: string) { this.updateDataForm.get('mail').setValue(username) };
  set passwordValue (username: string) { this.updateDataForm.get('password').setValue(username) };
  set confirmValue (username: string) { this.updateDataForm.get('confirm').setValue(username) };
  // set profileImgValue (username: string) { this.updateDataForm.get('profileImg').setValue(username) };

  enableModifies() {
    this.modifing = true;
    this.updateDataForm.enable();
  }

  submitValue() {
    this.attemptToSubmit = true;
    if (this.updateDataForm.invalid) {
      window.alert('Insered data are not correct');
      return;
    }
    this._resetSettings();
    this._setImage();
  }

  undoChanges() {
    this.usernameValue = this.currentUser.username;
    this.mailValue = this.currentUser.mail;
    this.passwordValue = this.currentUser.password;
    this.confirmValue = this.currentUser.password;
    this._setImage();
  }

  private _resetSettings() {
    let newUser: User = this._createUserFromData();
    this.userService.updateUser(newUser);
    sessionStorage.setItem('user', JSON.stringify(newUser));
    this.modifing = false;
    this.updateDataForm.disable();
  }

  private _createUserFromData(): User {
    let username = this.username.value;
    let mail = this.mail.value;
    let pass = this.password.value;
    let imageUrl: string = this._getFileNameFromPath();
    let roles = this.currentUser.roles;
    let token = `${ username }:${ pass }`;
    let id = this.currentUser.id;
    if (imageUrl == '') {
      imageUrl = this.currentUser.profileImageName;
    }
    return new User(username, mail, pass, imageUrl, roles, token, id);
  }

  private _setImage() {
    this.imageManager.loadImageFromServer().pipe(
      tap( image => {
        this.profileImage = image;
      })).subscribe();
  }

  private _getFileNameFromPath(): string {
    let path = this.profileImg.value;
    let name = path.substring(path.lastIndexOf('\\') + 1);
    return name;
  }
}
