import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProfileComponent } from './profile.component';
import { RouterModule, Routes } from '@angular/router'
import { CommonFormModule } from 'src/app/shared/common-form/common-form.module';

const routes: Routes = [
  { path: '', component: ProfileComponent },
]

@NgModule({
  declarations: [ProfileComponent],
  imports: [
    CommonModule,
    CommonFormModule,
    RouterModule.forChild(routes),
  ]
})
export class ProfileModule { }
