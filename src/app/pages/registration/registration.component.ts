import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { confirmPasswordValidator } from 'src/app/features/validators/confirm-password-validator';
import { UserClientService } from 'src/app/services/user-client/user-client.service';
import { User } from 'src/app/features/model/user';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  registrationForm: FormGroup;
  attemptToSubmit: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserClientService,
  ) { }

  ngOnInit() {
    this.registrationForm = this.formBuilder.group( {
      username: [ '', [ Validators.required ] ],
      mail: [ '', [ Validators.required, Validators.email ] ],
      password: [ '', [ Validators.required ] ],
      confirm: [ '', [ Validators.required ] ],
      profileImg: [ '' ],
    },
    {
      validators: [ confirmPasswordValidator() ]
    })
  }

  get username() { return this.registrationForm.get('username') };
  get mail() { return this.registrationForm.get('mail') };
  get password() { return this.registrationForm.get('password') };
  get confirm() { return this.registrationForm.get('confirm') };
  get profileImg() { return this.registrationForm.get('profileImg') };

  submitValue() {
    this.attemptToSubmit = true;
    if(this.registrationForm.valid) {
      this.userService.sendUser(this._createUserFromData());
    }
  }

  private _createUserFromData(): User {
    let user = this.username.value;
    let mail = this.mail.value;
    let pass = this.password.value;
    let imageUrl: string = this._getFileNameFromPath();
    return new User(user, mail, pass, imageUrl);
  }

  private _getFileNameFromPath(): string {
    let path = this.profileImg.value;
    let name = path.substring(path.lastIndexOf('\\') + 1);
    return name;
  }
}
