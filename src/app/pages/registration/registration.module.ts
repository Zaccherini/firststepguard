import { NgModule } from '@angular/core';
import { RegistrationComponent } from './registration.component';
import { RouterModule, Routes } from '@angular/router';
import { InitialNavigationModule } from 'src/app/shared/initial-navigation/initial-navigation.module';
import { CommonFormModule } from 'src/app/shared/common-form/common-form.module';

const routes: Routes = [
  { path: '', component: RegistrationComponent },
]

@NgModule({
  declarations: [RegistrationComponent],
  imports: [
    InitialNavigationModule,
    CommonFormModule,
    RouterModule.forChild(routes),
  ]
})
export class RegistrationModule { }
