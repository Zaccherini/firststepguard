import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms'

import { LoggerService } from 'src/app/services/logger/logger.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  attemptToSubmit: boolean;

  constructor(
    private formBuilder: FormBuilder,
    private logger: LoggerService,
  ) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group( {
      username: ['', Validators.required],
      password: ['', Validators.required],
    });
  }

  get username() { return this.loginForm.get('username') };
  get password() { return this.loginForm.get('password') };

  submitData() {
    this.attemptToSubmit = true;
    if (this.loginForm.valid) {
      this.logger.login(this.username.value, this.password.value);
    }
  }

}
