import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'
import { LoginComponent } from './login.component';
import { InitialNavigationModule } from 'src/app/shared/initial-navigation/initial-navigation.module';
import { CommonFormModule } from 'src/app/shared/common-form/common-form.module';

const routes: Routes = [
  { path: '', component: LoginComponent },
]

@NgModule({
  declarations: [
    LoginComponent,
  ],
  imports: [
    CommonFormModule,
    RouterModule.forChild(routes),
    InitialNavigationModule,
  ]
})
export class LoginModule { }
