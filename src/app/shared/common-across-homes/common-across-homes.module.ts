import { NgModule } from '@angular/core';
import { SidebarModule } from 'src/app/features/components/sidebar/sidebar.module';



@NgModule({
  declarations: [],
  imports: [
    SidebarModule,
  ],
  exports: [
    SidebarModule,
  ],
})
export class CommonAcrossHomesModule { }
