import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TopRightButtonsModule } from 'src/app/features/components/top-right-buttons/top-right-buttons.module';



@NgModule({
  imports: [
    CommonModule,
    TopRightButtonsModule,
  ],
  exports: [
    CommonModule,
    TopRightButtonsModule,
  ]
})
export class InitialNavigationModule { }
