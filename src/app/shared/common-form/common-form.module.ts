import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { InvalidParagraphModule } from 'src/app/features/components/invalidParagraph/invalid-paragraph.module';



@NgModule({
  declarations: [],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    InvalidParagraphModule,
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    InvalidParagraphModule
  ],
})
export class CommonFormModule { }
