import { TestBed } from '@angular/core/testing';

import { UserClientService } from './user-client.service';

describe('UserClientService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UserClientService = TestBed.get(UserClientService);
    expect(service).toBeTruthy();
  });
});
