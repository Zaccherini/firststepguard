import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../../features/model/user';
import { map } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})

export class UserClientService {

  private url: string = "http://localhost:3000/";
  constructor(private httpClient: HttpClient) { }

  sendUser(user: User) {
    this.httpClient.post(`${ this.url }api/users`, user).subscribe();
  }

  updateUser(user: User) {
    this.httpClient.put(`${ this.url }api/users/${ user.id }`, user).subscribe();
  }

  getUser(token: string) {
    return this.httpClient.get<User[]>(`${ this.url }api/users`).pipe(map( users => {
      return users.filter(user => {
        return user.token == token });
    }));
  }

  getUserProfileImage(imgName: string) {
    return this.httpClient.get(`${ this.url }img/${ imgName }`,
      { responseType: 'blob' } );
  }
}
