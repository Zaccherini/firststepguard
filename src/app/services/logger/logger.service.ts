import { Injectable } from '@angular/core';
import { UserClientService } from '../user-client/user-client.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  logged: boolean = false;

  constructor(
    private userClient: UserClientService,
    private router: Router,
  ) { }

  login(username: string, password: string) {
    let token = `${username}:${password}`;
    this.userClient.getUser(token).subscribe(result => {
      if ( result.length != 1 ) {
        window.alert('Username o password non corretti');
        return;
      }
      this.logged = true;
      this._setSessionStorage(result[0]);
      this.router.navigate(['mainPage']);
    });
  }

  logout() {
    this.logged = false;
    this.router.navigate(['login']);
  }

  _setSessionStorage(user) {
    sessionStorage.setItem('user', JSON.stringify(user));
    sessionStorage.setItem('currentRole', user.roles[0]);
  }
}
