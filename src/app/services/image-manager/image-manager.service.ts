import { Injectable } from '@angular/core';
import { User } from 'src/app/features/model/user';
import { UserClientService } from '../user-client/user-client.service';
import { Observable, of, fromEvent, throwError } from 'rxjs';
import { map, tap, flatMap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ImageManagerService {

  constructor(
    private userService: UserClientService,
  ) { }

  public loadImageFromServer() : Observable<string> {
    let currentUser: User = JSON.parse(sessionStorage.getItem('user'));
    if (!currentUser.profileImageName) {
      return throwError("Image not available");
    }
    return this.userService.getUserProfileImage(currentUser.profileImageName)
      .pipe(flatMap(data => {
        return this._createImageFromBlob(data);
      }));
    }

  private _createImageFromBlob(image: Blob): Observable<string> {
    let reader = new FileReader();
    if (image) {
      reader.readAsDataURL(image);
      return fromEvent(reader, 'load').pipe(map(event => {
        return (event.currentTarget as FileReader).result as string;
      }));
    }
    return throwError("Error download image from server");
  }

  // private _createImageFromBlob(image: Observable<Blob>) {
  //   let reader = new FileReader();
  //   reader.addEventListener("load", () => {
  //      this.profileImage = reader.result;
  //   }, false);
  //   if (image) {
  //      reader.readAsDataURL(image);
  //   }
  // }
}
