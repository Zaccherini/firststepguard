import { TestBed } from '@angular/core/testing';

import { ImageManagerService } from './image-manager.service';

describe('ImageManagerService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ImageManagerService = TestBed.get(ImageManagerService);
    expect(service).toBeTruthy();
  });
});
